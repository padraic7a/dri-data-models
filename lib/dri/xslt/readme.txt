This folder contains a set of XSLT transformation scripts for transforming XML metadata
using DC, MODS, EAD or MARC-XML into OAI-DC metadata.

At the moment they are not being used as the idea is to migrate models to using RDF properties.