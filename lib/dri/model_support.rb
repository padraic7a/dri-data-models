module DRI
  module ModelSupport
    autoload :Collections, 'dri/model_support/collections'
    autoload :Files, 'dri/model_support/files'
    autoload :InterchangeableMetadata, 'dri/model_support/interchangeable_metadata'
    autoload :EadSupport, 'dri/model_support/ead_support'
    autoload :ModsSupport, 'dri/model_support/mods_support'
    autoload :MarcSupport, 'dri/model_support/marc_support'
    autoload :Permissions, 'dri/model_support/permissions'
    autoload :Properties, 'dri/model_support/properties'
    autoload :RelationshipsSupport, 'dri/model_support/relationships_support'
  end
end
