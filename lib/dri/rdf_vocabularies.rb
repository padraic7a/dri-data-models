module DRI
  module RDFVocabularies
    autoload :DriRelsVocabulary, 'dri/rdf_vocabularies/dri_rels_vocabulary'
    autoload :ModsRelsVocabulary, 'dri/rdf_vocabularies/mods_rels_vocabulary'
    autoload :MarcRelatorsVocabulary, 'dri/rdf_vocabularies/marc_relators_vocabulary'
  end
end
